<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.08.18
 * Time: 16:49
 */

namespace altairvr\GeoJson;


class BaseGeometry
{

    /**
     * @var string $type
     */
    protected $type;
    /**
     * @var array $coordinates
     */
    protected $coordinates;


    public function asArray()
    {
        return [
            'type' => $this->type,
            'coordinates' => $this->coordinates
        ];
    }
}