<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.08.18
 * Time: 15:05
 */

namespace altairvr\GeoJson;


use altairvr\GeoJson\GeometryInterface;

class Feature
{
    const GEOJSON_TYPE = 'Feature';

    /**
     * @var BaseGeometry|GeometryCollection $geometry
     */
    protected $geometry;

    /**
     * @var array $property
     */
    protected $property;

    /**
     * @var string
     */
    protected $type;

    /**
     * @param mixed $name
     */
    public function __construct()
    {
        $this->type = self::GEOJSON_TYPE;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $key Property name
     * @param string $value Property value
     */

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function getBelong()
    {
        return $this->property['belong'];
    }

    /**
     * @return BaseGeometry|GeometryCollection
     */
    public function getGeometry()
    {
        return $this->geometry;
    }


    public function setProperty(array $properties)
    {
        $this->property = $properties;
    }


    /**
     * @param  $geometry
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;
    }

    public function setBelong($flag)
    {
        $this->belongs = $flag;
    }

    public function asJson()
    {
        return json_encode([
            'type' => $this->type,
            'properties' => $this->property,
            'geometry' => $this->geometry->asArray(),
        ], JSON_UNESCAPED_UNICODE);
    }

    public function asArray()
    {
        return [
            'type' => $this->type,
            'properties' => $this->property,
            'geometry' => $this->geometry->asArray(),
        ];
    }


}