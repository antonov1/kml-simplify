<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.08.18
 * Time: 17:39
 */

namespace altairvr\GeoJson;


use Location\Coordinate;

class Geofence
{
    /**
     * @var string GeoJson
     */
    private $layer;

    /**
     * @var Coordinate $coordinate
     */
    private $coordinate;

    /**
     * Geofence constructor.
     * @param Coordinate $coordinate
     * @param string $layer
     */
    public function __construct(Coordinate $coordinate, $layer)
    {
        $this->coordinate = $coordinate;
        $this->layer = json_decode($layer, true);
    }

    /**
     * Method determine contained point in geojson layer or not. Type of geojson : Feature, FeatureCollection.
     * @see https://tools.ietf.org/html/rfc7946
     * @return bool
     * @throws \Exception
     */
    public function contains()
    {
        switch ($this->layer['type']) {
            case "FeatureCollection" :
                 foreach ($this->layer['features'] as $item) {
                     if ($this->geometry($item['geometry'])) {
                         return true;
                     }
                  }
                 break;
            case "Feature":
                return $this->geometry($this->layer['geometry']);
            default:
                throw new \Exception('Format is not valid');
        }
    }

    /**
     * Method determine contained point in geometry or not. Type of geometry : Polygon, MultiPolygon GeometryCollection.
     * @param $geometry
     * @return array|bool
     */
    private function geometry($geometry)
    {
        switch ($geometry['type']) {
            case 'Polygon' :
                return $this->polygon($geometry);
            case 'GeometryCollection' :
                return $this->geometryCollection($geometry);
        }
    }


    /**
     * Method determine contained point in geometry collection or not. Type of geometry : Polygon, MultiPolygon.
     * @param $collection
     * @return array|bool
     */
    private function geometryCollection($collection)
    {
        foreach ($collection['geometries'] as $item) {
            if ($this->geometry($item) === true) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method determine contained point in polygon or not.
     * @param $polygon
     * @return bool
     */
    private function polygon($polygon)
    {
        $inside = true;
        foreach ($polygon['coordinates'] as $border) {
            $subpolygon = new \Location\Polygon();
            foreach ($border as $coordinate) {
                $subpolygon->addPoint(new Coordinate($coordinate[1], $coordinate[0]));
            }
            if ($inside !== $subpolygon->contains($this->coordinate)) {
                return false;
            }
            $inside = false;
        }
        return true;
    }


}