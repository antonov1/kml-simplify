<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.08.18
 * Time: 15:04
 */

namespace altairvr\GeoJson;


class FeatureCollection
{
    const GEOJSON_TYPE = 'FeatureCollection';
    /**
     * @var Feature[] $features Array of features
     */
    protected $features;

    public function __construct()
    {
        $this->type = self::GEOJSON_TYPE;
    }

    /**
     * @param Feature[] $features
     */

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function setFeature(array $features)
    {
        $this->features = $features;
    }

    public function addFeature(Feature $feature)
    {
        $this->features[] = $feature;
    }

    public function asJson()
    {
        $jsonFeatures = [];
        foreach ($this->features as $feature) {
            $jsonFeatures[] = $feature->asArray();
        }
        return json_encode(
            [
                'type' => $this->type,
                'features' => $jsonFeatures
            ]
            , JSON_UNESCAPED_UNICODE);
    }
}