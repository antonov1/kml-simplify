<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.08.18
 * Time: 15:16
 */

namespace altairvr\GeoJson;


class LineString extends BaseGeometry
{
    const GEOMETRY_TYPE = 'LineString';

    public function __construct()
    {
        $this->type = self::GEOMETRY_TYPE;
    }

    public function setCoordinate($coordinate)
    {
        $this->coordinates = $coordinate;
    }

    public function getCoordinate()
    {
        return $this->coordinates;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::GEOMETRY_TYPE;
    }

    /**
     * Return object as JSON
     *
     * @return string
     */
    public function asJson()
    {
        return json_encode([
            'type' => self::GEOMETRY_TYPE,
            'coordinates' => $this->coordinates
        ], JSON_UNESCAPED_UNICODE);
    }
}