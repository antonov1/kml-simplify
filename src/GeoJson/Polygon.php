<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.08.18
 * Time: 15:16
 */

namespace altairvr\GeoJson;


class Polygon extends BaseGeometry
{

    const GEOMETRY_TYPE = 'Polygon';

    public function __construct()
    {
        $this->type = 'Polygon';
    }

    public function setCoordinate($coordinate)
    {
        $this->coordinates[] = $coordinate;
    }

    public function getCoordinate()
    {
        return $this->coordinates;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Return object as JSON
     *
     * @return string
     */
    public function asJson()
    {
        return json_encode([
            'type' => $this->type,
            'coordinates' => $this->coordinates
        ], JSON_UNESCAPED_UNICODE);
    }
}