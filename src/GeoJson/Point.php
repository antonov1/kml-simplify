<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.08.18
 * Time: 15:16
 */

namespace altairvr\GeoJson;


/**
 * Class Point
 * @package altairvr\GeoJson
 */
class Point extends BaseGeometry
{
    const GEOMETRY_TYPE = 'Point';

    public function __construct()
    {
        $this->type = 'Point';
    }

    /**
     * @param array $coordinate
     * @return mixed|void
     */
    public function setCoordinate($coordinate)
    {
        $this->coordinates = $coordinate;
    }
}