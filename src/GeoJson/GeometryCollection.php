<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.08.18
 * Time: 10:22
 */

namespace altairvr\GeoJson;


class GeometryCollection
{
    const GEOMETRY_TYPE = 'GeometryCollection';

    /**
     * @var array $geometries
     */
    private $geometries;

    public function setGeometry(BaseGeometry $geometry)
    {
        $this->geometries[] = $geometry;
    }


    /**
     * @return array
     */
    public function asArray()
    {

        foreach ($this->geometries as $key => $geometry) {
            $this->geometries[$key] = $geometry->asArray();
        }
        return [
            'type' => self::GEOMETRY_TYPE,
            'geometries' => $this->geometries
        ];
    }
}