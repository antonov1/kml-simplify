<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.08.18
 * Time: 14:21
 */

namespace altairvr\KML;

use Location\Coordinate;
use Location\Polyline;
use Location\Processor\Polyline\SimplifyDouglasPeucker;

class KmlSimplified
{
    /**
     * @var integer $tolerance The perpendicular distance threshold in meters
     */
    private $tolerance;
    private $file;
    private $outputFile;
    private $minNumberCoordinates;

    /**
     * @param  string $file Path to file
     * @param  string $outputFile Name of results file
     * @param int $tolerance
     * @param int $minNumberCoordinates
     * @return string
     */
    public function __construct($file, $outputFile, $tolerance = 100, $minNumberCoordinates = null)
    {
        $this->file = $file;
        $this->outputFile = $outputFile;
        $this->tolerance = $tolerance;
        $this->minNumberCoordinates = $minNumberCoordinates;
    }

    public function run()
    {
        $kml = new \SimpleXMLElement(file_get_contents($this->file, FILE_USE_INCLUDE_PATH));

        if (isset($kml->Document->Folder)) {
            foreach ($kml->Document->Folder as $folder) {
                $folders[] = $this->folder($folder);
            }
            @$kml->Document->Folder->addChild('Folder', $folders);
        } else {
            foreach ($kml->Document->Placemark as $placemark) {
                $placemarks[] = $this->placemark($placemark);
            }
            @$kml->Document->addChild('Placemark', $placemarks);
        }

        file_put_contents($this->outputFile, $kml->asXML());
        return $this->outputFile;
    }

    public function folder($folder)
    {
        $plaecmarks = [];
        foreach ($folder->Placemark as $placecmark) {
            $plaecmarks[] = $this->placemark($placecmark);
        }
        @$folder->Placemarks->addChild('Placemark', $plaecmarks);
        return $folder->Placemark;
    }

    public function placemark($placemak)
    {
        $polygons = [];
        if (isset($placemak->MultiGeometry)) {
            foreach ($placemak->MultiGeometry->Polygon as $key => $polygon) {
                $polgg = $this->polygon($polygon);
                if ($polgg) {
                    $polygons[] = clone $polgg;
                }
            }
            unset($placemak->MultiGeometry);
            $placemak->addChild('MultiGeometry');
            foreach ($polygons as $pol) {
                $pg = $placemak->MultiGeometry->addChild('Polygon');
                $pg->addChild('outerBoundaryIs')->addChild('LinearRing')->addChild('coordinates', $pol->outerBoundaryIs->LinearRing->coordinates);
                if (isset($pol->innerBoundaryIs)) {
                    foreach ($pol->innerBoundaryIs as $ib) {
                        if ($pol->innerBoundaryIs->LinearRing->coordinates === null) {
                            continue;
                        }
                        $in = $pg->addChild('innerBoundaryIs');
                        $inlr = $in->addChild('LinearRing');
                        $inlr->addChild('coordinates', $pol->innerBoundaryIs->LinearRing->coordinates);
                    }
                }
            }
            return $placemak;
        } else {
            return $this->polygon($placemak->Polygon);
        }
    }


    public function polygon(\SimpleXMLElement $polygon)
    {
        if (isset($polygon->outerBoundaryIs->LinearRing)) {
            $coordinates = $this->simplifyCoordinate($polygon->outerBoundaryIs->LinearRing->coordinates, true);
            if (!$coordinates) {
                return null;
            }
            $polygon->outerBoundaryIs->LinearRing->coordinates = $coordinates;
            if (isset($polygon->innerBoundaryIs)) {
                foreach ($polygon->innerBoundaryIs as $boundry) {
                    $boundry->LinearRing->coordinates = $this->simplifyCoordinate($boundry->LinearRing->coordinates, false);
                }
            }
            return $polygon;
        }
        if (isset($polygon->outerBoundaryIs->LineString)) {
            $coordinates = $this->simplifyCoordinate($polygon->outerBoundaryIs->LineString->coordinates, true);
            if (!$coordinates) {
                return null;
            }
            $polygon->outerBoundaryIs->LineString->coordinates = $coordinates;
        }
        return $polygon;
    }

    /**
     * @param $coordinate String of coordinates the first number is longitude, the last is latitude.
     * @return string String of simplified string
     */
    /*TODO Заменить на флаг или разбить по разным методам  */
    public function simplifyCoordinate($coordinate, $skipMicropolygon = false)
    {
        $tmp = preg_split('#\s#', $coordinate, null, PREG_SPLIT_NO_EMPTY);
        $polyline = new Polyline();
//        If number of coordinate less than minNumberCoordinates return null;
        foreach ($tmp as $key => $val) {
            $coordinates = explode(',', $val);
            if ($key === (count($tmp) - 1)) {  //Remove last coordinate because sometimes the first and the last coordinate have the same value. And Douglas-Peucker algorithm will return only two points.
                $last = $coordinates;
                continue;
            }
            $polyline->addPoint(new Coordinate(floatval($coordinates[1]), floatval($coordinates[0])));
        }

        $processor = new SimplifyDouglasPeucker($this->tolerance);
        $simplified = $processor->simplify($polyline);
        $row = [];
        $simplified->addPoint((new Coordinate(floatval($last[1]), floatval($last[0]))));
        $points = count($simplified->getPoints());
        if ($this->minNumberCoordinates !== null && $skipMicropolygon === true && $points <= $this->minNumberCoordinates) {
            return null;
        }
        foreach ($simplified->getPoints() as $point) {
            $row[] = $point->getLng() . ',' . $point->getLat();
        }
        return PHP_EOL . implode(PHP_EOL, $row) . PHP_EOL;
    }
}