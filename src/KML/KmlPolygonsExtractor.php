<?php

namespace altairvr\KML;

class KmlPolygonsExtractor
{
    private $file;

    /**
     * @var array $polygons
     */
    private $polygons = [];

    /**
     * KmlToJson constructor.
     * @param string $file path to kml file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Validate kml file
     * @throws \Exception
     */
    public function validate()
    {
        if (!is_file($this->file)) {
            throw new \Exception('File not exist');
        }
        if (!mime_content_type($this->file) === 'application/vnd.google-earth.kml+xml') {
            throw new \Exeption('File is not KML');
        }
    }

    /**
     * Converting KML structure to GeoJSON format
     * @return false|string
     * @throws \Exception
     */
    public function extract()
    {
        $this->validate();
        $kml = new \SimpleXMLElement(file_get_contents($this->file, FILE_USE_INCLUDE_PATH));
        if ($kml->Document->Folder) {
            foreach ($kml->Document->Folder as $folder) {
                $this->parseFolder($folder);
            }
        } else {
            foreach ($kml->Document->Placemark as $placemark) {
                $this->parsePlacemark($placemark);
            }
        }
        return $this->polygons;
    }


    private function parseFolder(\SimpleXMLElement $folder)
    {
        foreach ($folder->Placemark as $placemark) {
            $this->parsePlacemark($placemark);
        }
    }

    private function parsePlacemark(\SimpleXMLElement $placemark)
    {
        if ($placemark->MultiGeometry) {
            $this->parseMultiGeometry($placemark->MultiGeometry);
        } elseif ($placemark->Polygon) {
            $this->polygons[] = $this->getPolygon($placemark->Polygon);
        } elseif ($placemark->LineString) {
            $this->polygons[] = $this->parseLineString($placemark->LineString);
        }
        return;
    }

    public function parseLineString(\SimpleXMLElement $line)
    {
        return $line->coordinates->__toString();
    }

    /**
     * @param \SimpleXMLElement $polygon
     * @return \altairvr\GeoJson\Polygon
     */
    private function getPolygon(\SimpleXMLElement $polygon)
    {
        return $polygon->outerBoundaryIs->LinearRing->coordinates->__toString();
    }

    private function parseMultiGeometry(\SimpleXMLElement $multiGeometry)
    {
        if (count($multiGeometry->children()) !== 0) {
            foreach ($multiGeometry->children() as $item) {
                switch ($item->getName()) {
                    case 'Polygon' :
                        $this->polygons[] = $this->getPolygon($item);
                        break;
                    default:
                        continue;
                }
            }
        } else {
            throw new \KmlStructureException('MultiGeometry has not any child node. Kml file is not correct');
        }
    }
}