<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 1/17/19
 * Time: 10:33 AM
 */

namespace altairvr\KML;

use altairvr\GeoJson\Polygon;
use Location\Coordinate;

class Subtraction
{
    private $outer;
    private $inner;
    private $innerPolygons;
    private $outputFile;

    public function __construct($outer, $inner, $outputFile)
    {
        $this->inner = $inner;
        $this->outer = $outer;
        $this->outputFile = $outputFile;
    }

    /**
     * Validate kml file
     * @throws \Exception
     */
    public function validate()
    {
        if (!is_file($this->inner)) {
            throw new \Exception('Inner file not exist');
        }
        if (!mime_content_type($this->inner) === 'application/vnd.google-earth.kml+xml') {
            throw new \Exception('Inner file is not KML');
        }
        if (!is_file($this->outer)) {
            throw new \Exception('Outer file not exist');
        }
        if (!mime_content_type($this->outer) === 'application/vnd.google-earth.kml+xml') {
            throw new \Exception('Outer file is not KML');
        }
    }

    public function run()
    {
        $this->validate();
        $extractor = new KmlPolygonsExtractor($this->inner);
        $this->innerPolygons = $extractor->extract();
        $kml = new \SimpleXMLElement(file_get_contents($this->outer, FILE_USE_INCLUDE_PATH));

        if (isset($kml->Document->Folder)) {
            foreach ($kml->Document->Folder as $folder) {
                $folders[] = $this->folder($folder);
            }
            @$kml->Document->Folder->addChild('Folder', $folders);
        } else {
            foreach ($kml->Document->Placemark as $placemark) {
                $placemarks[] = $this->placemark($placemark);
            }
            @$kml->Document->addChild('Placemark', $placemarks);
        }

        file_put_contents($this->outputFile, $kml->asXML());
        return $this->outputFile;
    }


    public function folder($folder)
    {
        $plaecmarks = [];
        foreach ($folder->Placemark as $placecmark) {
            $plaecmarks[] = $this->placemark($placecmark);
        }
        @$folder->Placemarks->addChild('Placemark', $plaecmarks);
        return $folder->Placemark;
    }

    public function placemark($placemak)
    {
        $polygons = [];
        if (isset($placemak->MultiGeometry)) {
            foreach ($placemak->MultiGeometry->Polygon as $key => $polygon) {
                $polgg = $this->polygon($polygon);
                if ($polgg) {
                    $polygons[] = clone $polgg;
                }
            }
            unset($placemak->MultiGeometry);
            $placemak->addChild('MultiGeometry');
            foreach ($polygons as $pol) {
                $pg = $placemak->MultiGeometry->addChild('Polygon');
                $pg->addChild('outerBoundaryIs')->addChild('LinearRing')->addChild('coordinates', $pol->outerBoundaryIs->LinearRing->coordinates);
                if (isset($pol->innerBoundaryIs)) {
                    foreach ($pol->innerBoundaryIs as $ib) {
                        if ($pol->innerBoundaryIs->LinearRing->coordinates === null) {
                            continue;
                        }
                        $in = $pg->addChild('innerBoundaryIs');
                        $inlr = $in->addChild('LinearRing');
                        $inlr->addChild('coordinates', $pol->innerBoundaryIs->LinearRing->coordinates);
                    }
                }
            }
            return $placemak;
        } elseif ($placemak->Polygon) {
            return $this->polygon($placemak->Polygon);
        } elseif ($placemak->LineString) {
            $coordinates = $placemak->LineString->coordinates->__toString();
            unset($placemak->LineString);
            $placemak->
            addChild('Polygon')->
            addChild('outerBoundaryIs')->
            addChild('LinearRing')->
            addChild('coordinates', $coordinates);
            return $this->polygon($placemak->Polygon);
        }
    }

    public function lineString(\SimpleXMLElement $lineString)
    {
        $ret = $this->subtract($lineString->coordinates);
        return $ret;
    }


    public function polygon(\SimpleXMLElement $polygon)
    {
        if (isset($polygon->outerBoundaryIs->LinearRing)) {
            $r = $polygon->outerBoundaryIs->LinearRing->coordinates;
            $innerPolygons = $this->subtract($polygon->outerBoundaryIs->LinearRing->coordinates);
            if ($innerPolygons) {
                foreach ($innerPolygons as $innerPolygon) {
                    $polygon->addChild('innerBoundaryIs')
                        ->addChild('LinearRing')
                        ->addChild('coordinates', $innerPolygon);
                }
            }
            return $polygon;
        }
        if (isset($polygon->outerBoundaryIs->LineString)) {
            $coordinates = $this->subtract($polygon->outerBoundaryIs->LineString->coordinates, true);
            if (!$coordinates) {
                return null;
            }
            $polygon->outerBoundaryIs->LineString->coordinates = $coordinates;
        }
        return $polygon;
    }

    /**
     * @param $coordinate String of coordinates the first number is longitude, the last is latitude.
     * @return string String of simplified string
     */
    /*TODO Заменить на флаг или разбить по разным методам  */
    public function subtract($coordinate)
    {
        $return = [];
        //Переводим строку из координат в массив обЪектов Coordinate
        $outerBorderPoints = $this->splitKmlCoordinate($coordinate);

        // Создаем полигон из  координат
        $outerPolygon = $this->createPolygon($outerBorderPoints);

        //Проходим по всем внутренним полигонам и определяем входят ли они полностью во внешний полигон. Если входят,
//        то добавляем в массив
        foreach ($this->innerPolygons as $item) {
            $innerBorderPoints = $this->splitKmlCoordinate($item);
            $innerPolygon = $this->createPolygon($innerBorderPoints);

            //Входят ли все точки внутреннего полигона во внешний
            foreach ($innerBorderPoints as $coordinate) {
                if ($outerPolygon->contains($coordinate) === false) {
                    continue 2;
                }
            }

            //Входит ли хоть одна точка внешнего полигона во внутренний
            foreach ($outerBorderPoints as $coordinate) {
                if ($innerPolygon->contains($coordinate) === true) {
                    continue 2;
                }
            }
            $return[] = $item;
        }
        return $return;
    }


    private function isContain($coordinates, \Location\Polygon $polygon)
    {
        foreach ($coordinates as $coordinate) {
            if ($polygon->contains($coordinate)) {

            }
        }
        $return = [];
    }

    private function isNotContain($coordinates, \Location\Polygon $polygon)
    {
        foreach ($coordinates as $coordinate) {
            if (!$polygon->contains($coordinate)) {

            }
        }
        $return = [];
    }


    private function createPolygon($coordinates)
    {
        $polygon = new \Location\Polygon();
        foreach ($coordinates as $coordinate) {
            $polygon->addPoint($coordinate);
        }
        return $polygon;

    }


    public function splitKmlCoordinate($coordinatesString)
    {
        $return = [];
        $tmp = preg_split('#\s#', $coordinatesString, null, PREG_SPLIT_NO_EMPTY);
        foreach ($tmp as $key => $val) {
            $coordinates = explode(',', $val);
            $return[] = new Coordinate(floatval($coordinates[1]), floatval($coordinates[0]));
        }
        return $return;
    }

    private function convertLineStringToPolygon($lineString)
    {

    }


}