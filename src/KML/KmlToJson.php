<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.08.18
 * Time: 16:05
 */

namespace altairvr\KML;

use altairvr\GeoJson\Feature;
use altairvr\GeoJson\Point;

class KmlToJson
{
    private $file;

    /**
     * @var Feature[] $features
     */
    private $features;

    /**
     * KmlToJson constructor.
     * @param $file path to kml file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Validate kml file
     * @throws \Exception
     */
    public function validate()
    {
        if (!is_file($this->file)) {
            throw new \Exception('File not exist');
        }
        if (!mime_content_type($this->file) === 'application/vnd.google-earth.kml+xml') {
            throw new \Exeption('File is not KML');
        }
    }

    /**
     * Converting KML structure to GeoJSON format
     * @return false|string
     * @throws \Exception
     */
    public function convert()
    {
        $this->validate();
        $kml = new \SimpleXMLElement(file_get_contents($this->file, FILE_USE_INCLUDE_PATH));
        if ($kml->Document->Folder) {
            foreach ($kml->Document->Folder as $folder) {
                $this->parseFolder($folder);
            }
        } else {
            foreach ($kml->Document->Placemark as $placemark) {
                $this->parsePlacemark($placemark);
            }
        }

        if (count($this->features) > 1) {
            $geojson = new \altairvr\GeoJson\FeatureCollection();
            $geojson->setFeature($this->features);
            return $geojson->asJson();
        }
        return $this->features[0]->asJson();
    }


    private function parseFolder(\SimpleXMLElement $folder)
    {
        foreach ($folder->Placemark as $placemark) {
            $this->parsePlacemark($placemark);
        }
    }

    private function parsePlacemark(\SimpleXMLElement $placemark)
    {
        $name =  $placemark->name->__toString() ?: '';
        $description =  $placemark->description->__toString() ?: '';
        $feature = new \altairvr\GeoJson\Feature();
        $feature->setProperty([
                'name' => $name,
                'description' => $description,
            ]
        );
        if ($placemark->MultiGeometry) {
            $geometry = $this->parseMultiGeometry($placemark->MultiGeometry);
        } elseif ($placemark->Polygon) {
            $geometry = $this->parsePolygon($placemark->Polygon);
        } elseif ($placemark->LineString) {
            $geometry = $this->parseLineString($placemark->LineString);
        } else {
            return;
        }
        $feature->setGeometry($geometry);
        $this->features[] = $feature;
    }


    /**
     * @param \SimpleXMLElement $polygon
     * @return \altairvr\GeoJson\Polygon
     */
    private function parsePolygon(\SimpleXMLElement $polygon)
    {
        $polygonJson = new \altairvr\GeoJson\Polygon();
        $polygonJson->setCoordinate($this->convertCoordinatesStringToArray($polygon->outerBoundaryIs->LinearRing->coordinates->__toString()));
        foreach ($polygon->innerBoundaryIs as $innerBoundries) {
            $polygonJson->setCoordinate($this->convertCoordinatesStringToArray($innerBoundries->LinearRing->coordinates->__toString()));
        }
        return $polygonJson;
    }

    private function parseLineString(\SimpleXMLElement $lineString)
    {
        $lineStringJson = new \altairvr\GeoJson\Polygon();
        $lineStringJson->setCoordinate($this->convertCoordinatesStringToArray($lineString->coordinates->__toString()));
        return $lineStringJson;
    }

    private function parseMultiGeometry(\SimpleXMLElement $multiGeometry)
    {
        $geometryCollection = new \altairvr\GeoJson\GeometryCollection();
        if (count($multiGeometry->children()) !== 0) {
            foreach ($multiGeometry->children() as $item) {
                switch ($item->getName()) {
                    case 'Polygon' :
                        $geometryCollection->setGeometry($this->parsePolygon($item));
                        break;
                    default:
                        continue;
                }
            }
        } else {
            throw new \KmlStructureException('MultiGeometry has not any child node. Kml file is not correct');
        }
        return $geometryCollection;
    }

    private function convertCoordinatesStringToArray($boundries)
    {
        $tmp = preg_split('#\s*(,0(\.0)*)*\s#', $boundries);
        if (reset($tmp) === "") {
            array_shift($tmp);
        }
        if (trim(end($tmp)) === "") {
            array_pop($tmp);
        }
        $row = [];
        foreach ($tmp as $key => $val) {
            $coordinate = [];
            if ($val === "") {
                continue;
            }
            $tmp3 = explode(",", $val);
            $coordinate[] = floatval($tmp3[0]);
            $coordinate[] = floatval($tmp3[1]);
            $row[] = $coordinate;
        };
        return $row;
    }


}